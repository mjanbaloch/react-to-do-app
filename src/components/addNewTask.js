import React from "react";
import Button from "@material-ui/core";
import TextField from "@material-ui/core/TextField";

class AddNewTask extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input: "",
      task: [],
    };
  }

  onAddingTask = (event) => {
    this.setState({ task: event.target.value, input: event.target.value });
  };
  onFormSubmit = (event) => {
    event.preventDefault();
    this.props.whenTaskAdded([...this.props.state, this.state.task]);
    this.setState({ input: "" });
  };

  onDeleteAll = () => {
    if (this.props.state.length != 0) {
      let confrm = window.confirm("Are you sure?");
      if (confrm) {
        this.props.whenTaskAdded([]);
      }
    } else {
      alert("Ooops!, There No Task in The List");
    }
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onFormSubmit}>
          <TextField
            label=" Add New Task"
            onChange={this.onAddingTask}
            value={this.state.input}
          />
          <Button
            variant="contained"
            color="primary"
            type="submit"
            disableElevation={true}
          >
            Add
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={this.onDeleteAll}
          >
            Delete All
          </Button>
        </form>
      </div>
    );
  }
}

export default AddNewTask;
