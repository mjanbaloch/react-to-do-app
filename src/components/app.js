import React from "react";
import Header from "./header";
import TodoList from "./todoList";
import AddNewTaks from "./addNewTask";
import Card from "@material-ui/core/Card";
import CardactionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
    };
  }

  onAddTask = (task) => {
    this.setState({
      tasks: task,
    });
  };
  onClear = (cleared) => {
    this.setState({
      tasks: cleared,
    });
  };
  render() {
    console.log(this.state.tasks);
    return (
      <div>
        <Card>
          <CardactionArea>
            <CardContent>
              <Header />
              <TodoList task={this.state.tasks} onClear={this.onClear} />
              <AddNewTaks
                whenTaskAdded={this.onAddTask}
                state={this.state.tasks}
              />
            </CardContent>
          </CardactionArea>
        </Card>
      </div>
    );
  }
}

export default App;
