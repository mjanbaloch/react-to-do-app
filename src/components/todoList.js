import React from "react";

const TodoList = (props) => {
  const addTask = () => {
    return props.task.map((arr, index) => {
      return (
        <div key={index}>
          <span> {arr}</span>
          <button
            style={{ marginLeft: 34 }}
            onClick={() => {
              props.onClear(props.task.filter((task) => task != arr));
            }}
          >
            Clear
          </button>
        </div>
      );
    });
  };

  return <div>{addTask()}</div>;
};

export default TodoList;
